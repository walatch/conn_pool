import concurrent.futures
import time

import conn_pool
import db

initial_pool = input("Initial connection pool size (int)? [defaults to 5]: ")
initial_pool = 5 if not initial_pool else int(initial_pool)
min_pool = input("Min connection pool size (int)? [defaults to 2]: ")
min_pool = 2 if not min_pool else int(min_pool)
max_pool = input("Max connection pool size (int)? [defaults to 8]: ")
max_pool = 8 if not max_pool else int(max_pool)
wait_timer = input("Wait timer for free connection if pool is maxed out in seconds [defaults to 1]: ")
wait_timer = [1 for _ in range(100)] if not wait_timer else [float(wait_timer) for _ in range(100)]


connection_pool = conn_pool.ConnPool(db.get_conn, initial_pool, min_pool, max_pool)
task_query = """SELECT password FROM users"""
tasks = [task_id for task_id in range(1, 101)]


def sample_task(task_id, timer):
    conn = connection_pool.get_conn()
    while not conn:  # if connection not acquired, sleep for -timer- sec(s) and try again
        print(f"{'#' * 15} No free connection available for task {str(task_id).zfill(3)}\
         - retrying in {timer} second(s). {'#' * 15}")
        time.sleep(timer)
        conn = connection_pool.get_conn()
    print(f"\nTask {str(task_id).zfill(3)} acquired free connection ID: {conn.conn_id}")

    result = None
    with conn.connection:
        with conn.connection.cursor() as cursor:
            cursor.execute(task_query)
            result = cursor.fetchall()
            time.sleep(1)
    connection_pool.release_conn(conn)

    print(f"""\nTask {str(task_id).zfill(3)} released connection - result of fetch: {result} \
    unused : {connection_pool.num_of_free_conns()}  \
    total pool : {len(connection_pool.pool)} --""")

    if task_id % 15 == 0:
        print(f"\n{'-' * 30} Removing unused connections - was: {len(connection_pool.pool)}\
 - removed: {connection_pool.remove_unused_conns()} - now: {len(connection_pool.pool)} {'-' * 30}\n")
    return result


with concurrent.futures.ThreadPoolExecutor() as executor:
    fetches = executor.map(sample_task, tasks, wait_timer)
    num_of_confirmed_fetches = [1 for fetch in fetches if fetch]


print('\n')
print(f"{'-' * 30} Removing unused connections - was: {len(connection_pool.pool)}\
 - removed: {connection_pool.remove_unused_conns()} - now: {len(connection_pool.pool)} {'-' * 30}")
print('\n')

print(f"Confirmed fetches for {tasks[-1]} task(s): {sum(num_of_confirmed_fetches)}\n\n")
