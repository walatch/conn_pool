import threading
import itertools


class Connection:
    """ Individual Connection object
     attr: connection - :obj:
     attr: is_used - :bool:
     attr: conn_id - :int: - autoincrement from itertools.count """

    next_id = itertools.count()

    def __init__(self, conn_obj):
        self.connection = conn_obj()
        self.is_used = False
        self.conn_id = next(self.next_id)

    def set_as_used(self):
        self.is_used = True

    def set_as_unused(self):
        self.is_used = False

    def close_conn_and_delete(self):
        self.connection.close()


class ConnPool:
    def __init__(self, conn_func, initial_pool, min_pool, max_pool):
        self.initial_pool = initial_pool
        self.min_pool_size = min_pool
        self.max_pool_size = max_pool
        self.conn_func = conn_func
        self.pool = {Connection(self.conn_func) for _ in range(1, initial_pool + 1)}
        self.lock = threading.Lock()
        self.semaphore = threading.Semaphore()

    def get_conn(self):
        """
        Get unused connection from the pool and flag as used if available or try to create new and flag as used
        unless pool is maxed (return False)
        """
        self.lock.acquire()
        for conn in self.pool:
            if not conn.is_used:
                conn.set_as_used()
                self.lock.release()
                return conn
        if not self.__is_max_pool():
            new_conn = self.__create_conn()
            new_conn.set_as_used()
            self.lock.release()
            return new_conn
        self.lock.release()
        return False

    def remove_unused_conns(self):
        """ Check for unused connections and remove them down to minimum amount (min_pool_size)"""
        self.lock.acquire()
        counter = 0
        unused_pool = set()
        for conn in self.pool:
            if not conn.is_used and len(self.pool) - counter - 1 >= self.min_pool_size:
                counter += 1
                unused_pool.add(conn)
                conn.close_conn_and_delete()
        self.pool -= unused_pool
        self.lock.release()
        return counter

    def num_of_free_conns(self):
        """ Returns number :int: of free connections in pool """
        self.lock.acquire()
        result = sum(not conn.is_used for conn in self.pool)
        self.lock.release()
        return result

    def release_conn(self, conn):
        """ Sets connection as unused """
        conn.set_as_unused()

    def __create_conn(self):
        """ Creates new connection, adds to the pool and returns that connection flagged as used """
        self.semaphore.acquire()
        new_conn = Connection(self.conn_func)
        self.pool.add(new_conn)
        self.semaphore.release()
        return new_conn

    def __is_max_pool(self):
        """ Return True if maximum pool size is reached, else False"""
        return len(self.pool) >= self.max_pool_size
