import psycopg2
from configparser import ConfigParser


class DB:
    def __init__(self, user, password, host, port, database):
        self.db_conn = psycopg2.connect(
            user=user,
            password=password,
            host=host,
            port=port,
            database=database
        )


def get_config_info():
    cp = ConfigParser()
    cp.read("config.ini")
    return (
        cp["DATABASE"]["USER"],
        cp["DATABASE"]["PASSWORD"],
        cp["DATABASE"]["HOST"],
        cp["DATABASE"]["PORT"],
        cp["DATABASE"]["DATABASE"],
    )


def get_conn():
    db_obj = DB(*get_config_info())
    return db_obj.db_conn
